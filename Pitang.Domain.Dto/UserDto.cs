﻿using System;
using System.Collections.Generic;

namespace Pitang.Domain.Dto
{
    public class UserDto
    {
        public UserDto(string firstName, string lastName, string email, string password, List<PhoneDto> phone)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Password = password;            
            Phone = phone;
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastLogin { get; set; }
        public List<PhoneDto> Phone { get; set; }
    }
}
