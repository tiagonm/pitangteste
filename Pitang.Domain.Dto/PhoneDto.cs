﻿namespace Pitang.Domain.Dto
{
    public class PhoneDto
    {
        public PhoneDto(int number, int areaCode, string countryCode)
        {
            Number = number;
            AreaCode = areaCode;
            CountryCode = countryCode;
        }

        public int Id { get; set; }
        public int Number { get; set; }
        public int AreaCode { get; set; }
        public string CountryCode { get; set; }        
    }
}
