﻿using Pitang.Application.Messages.Base;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Pitang.Application.Messages.Message
{
    public class UserMessageResponse: BaseMessageResponse
    {
        [DataMember(Name = "firstName")]        
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]        
        public string LastName { get; set; }

        [DataMember(Name = "email")]        
        public string Email { get; set; }

        [DataMember(Name = "password")]        
        public string Password { get; set; }

        [DataMember(Name = "phones")]        
        public List<PhoneMessageRequest> Phones { get; set; }

        [DataMember(Name = "created_at")]
        public DateTime Created_At { get; set; }

        [DataMember(Name = "last_login")]
        public DateTime? Last_Login { get; set; }
    }
}
