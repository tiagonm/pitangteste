﻿using Pitang.Application.Messages.Base;
using System.Runtime.Serialization;

namespace Pitang.Application.Messages.Message
{
    public class PhoneMessageRequest: BaseMessageRequest
    {
        [DataMember(Name = "number")]
        public int Number { get; set; }

        [DataMember(Name = "area_code")]
        public int Area_Code { get; set; }

        [DataMember(Name = "country_code")]
        public string Country_Code { get; set; }
    }
}