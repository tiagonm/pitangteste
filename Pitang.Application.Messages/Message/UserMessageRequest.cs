﻿using Pitang.Application.Messages.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace Pitang.Application.Messages.Message
{
    public class UserMessageRequest: BaseMessageRequest
    {
        [DataMember(Name ="firstName")]
        [Required(ErrorMessage = "Missing fields")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        [Required(ErrorMessage = "Missing fields")]
        public string LastName { get; set; }

        [DataMember(Name = "email")]
        [Required(ErrorMessage = "Missing fields")]
        public string Email { get; set; }

        [DataMember(Name = "password")]
        [Required(ErrorMessage = "Missing fields")]
        public string Password { get; set; }

        [DataMember(Name = "phones")]
        [Required(ErrorMessage = "Missing fields")]
        public List<PhoneMessageRequest> Phones { get; set; }
    }
}
