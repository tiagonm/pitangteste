﻿using Pitang.Application.Messages.Base;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Pitang.Application.Messages.Message
{
    public class SiginMessageResponse: BaseMessageResponse
    {
        [DataMember(Name ="access_token")]
        public string AccessToken { get; set; }
    }
}
