﻿using Pitang.Application.Messages.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace Pitang.Application.Messages.Message
{
    public class SiginMessageRequest : BaseMessageRequest
    {
        [DataMember(Name = "email")]
        [Required(ErrorMessage = "Missing fields")]
        public string Email { get; set; }

        [DataMember(Name = "password")]
        [Required(ErrorMessage = "Missing fields")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid fields")]
        public string Password { get; set; }
    }
}
