﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Pitang.Application.Messages.Base
{
    public abstract class BaseMessageResponse
    {
        [DataMember(Name ="message")]
        public string Message { get; set; }

        [DataMember(Name = "errorCode")]
        public int? ErrorCode { get; set; }
    }
}
