﻿using Pitang.Infrastucture.Repository.Implementation;
using Pitang.Infrastucture.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Pitang.Infrastucture.Repository.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly PitangDbContext _context;
        public IUserRepository UsuarioRepository { get; private set; }

        public UnitOfWork(PitangDbContext context)
        {
            this._context = context;

            this.UsuarioRepository = new UserRepository(_context);
        }
        
        public int Complete() => _context.SaveChanges();

        public async Task<int> CompleteAsync() => await _context.SaveChangesAsync();

        public void Dispose() => _context.Dispose();

    }
}
