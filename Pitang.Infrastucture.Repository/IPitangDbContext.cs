﻿using Microsoft.EntityFrameworkCore;
using Pitang.Domain.Model;
using System;

namespace Pitang.Infrastucture.Repository
{
    public interface IPitangDbContext : IDisposable
    {
        DbSet<User> Users { get; set; }
        DbSet<UserPhone> UserPhones { get; set; }
    }
}
