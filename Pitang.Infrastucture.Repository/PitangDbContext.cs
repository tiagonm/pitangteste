﻿using Microsoft.EntityFrameworkCore;
using Pitang.Domain.Model;

namespace Pitang.Infrastucture.Repository
{
    public class PitangDbContext: DbContext, IPitangDbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserPhone> UserPhones { get; set; }
        
        public PitangDbContext(DbContextOptions<PitangDbContext> options) : base(options)
        {
            
        }
    }
}
