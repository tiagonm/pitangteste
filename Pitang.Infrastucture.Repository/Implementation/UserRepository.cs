﻿using Microsoft.EntityFrameworkCore;
using Pitang.Domain.Model;
using Pitang.Infrastucture.Repository.Interface;
using System.Linq;
using System.Threading.Tasks;

namespace Pitang.Infrastucture.Repository.Implementation
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(PitangDbContext context) : base(context)
        {
        }

        public async Task<bool> GetByEmail(string email)
        {
            var user = from u in Context.Set<User>()
                       .Include(t => t.Phone)
                       .Where(w => w.Email == email)
                       select u;

            return await user.AnyAsync();
        }

        public async Task<User> GetById(int id)
        {
            var user = from u in Context.Set<User>()
                       .Include(t => t.Phone)
                       .Where(w => w.Id == id)
                       select u;

            return await user.SingleOrDefaultAsync();
        }

        public async Task<User> SignIn(string email, string passwordEncrypt)
        {
            var user = from u in Context.Set<User>()
                       .Include(t => t.Phone)
                       .Where(w => w.Email == email && w.Password == passwordEncrypt)
                       select u;

            return await user.SingleOrDefaultAsync();
        }
    }
}
