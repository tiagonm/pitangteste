﻿using System;
using System.Threading.Tasks;

namespace Pitang.Infrastucture.Repository.Interface
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository UsuarioRepository { get; }
        int Complete();
        Task<int> CompleteAsync();
    }
}
