﻿using System.Threading.Tasks;
using Pitang.Domain.Model;

namespace Pitang.Infrastucture.Repository.Interface
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> SignIn(string email, string passwordEncrypt);
        Task<bool> GetByEmail(string email);
        Task<User> GetById(int id);
    }
}
