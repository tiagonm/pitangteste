﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pitang.Domain.Model.Enum
{
    public enum ErrorCode
    {
        NotFound = 1,
        BusinessError = 2
    }
}
