﻿using Pitang.Domain.Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pitang.Domain.Model
{
    public class User: IModel
    {
        public User()
        {

        }
        public User(string firstName, string lastName, string email, string password, DateTime createdAt, DateTime? lastLogin, List<UserPhone> phone)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Password = password;
            CreatedAt = createdAt;
            LastLogin = lastLogin;
            Phone = phone;
        }

        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastLogin { get; set; }
        public List<UserPhone> Phone { get; set; }

        public static User Create(ModelResultValidation result, string email, string firstName, string lastName, string password, List<UserPhone> phone)
        {
            if (!result.IsValid) return null;

            ValidateFields(result, email, firstName, lastName, password, phone);

            if (result.IsValid)
            {
                return new User(firstName, lastName, email, password, DateTime.Now, null,phone);
            } else
            {
                return null;
            }
        }

        private static void ValidateFields(ModelResultValidation result, string email, string firstName, string lastName, string password, List<UserPhone> phone)
        {
            
        }
    }
}
