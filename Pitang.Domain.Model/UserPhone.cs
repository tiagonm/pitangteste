﻿using System;
using System.ComponentModel.DataAnnotations;
using Pitang.Domain.Model.Base;

namespace Pitang.Domain.Model
{
    public class UserPhone
    {
        public UserPhone()
        {

        }
        public UserPhone(int number, int areaCode, string countryCode)
        {
            Number = number;
            AreaCode = areaCode;
            CountryCode = countryCode;
        }

        [Key]
        public int Id { get; set; }
        public int Number { get; set; }
        public int AreaCode { get; set; }
        public string CountryCode { get; set; }
        public UserPhone User { get; set; }

        public static UserPhone Create(ModelResultValidation result, int areaCode, string countryCode, int number)
        {
            ValidateFields(result, number, areaCode, countryCode);

            if (result.IsValid)
            {
                return new UserPhone(number, areaCode, countryCode);
            }
            else
            {
                return null;
            }
        }

        private static void ValidateFields(ModelResultValidation result, int number, int areaCode, string countryCode)
        {

        }
    }
}
