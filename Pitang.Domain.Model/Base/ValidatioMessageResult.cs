﻿using Pitang.Domain.Model.Enum;

namespace Pitang.Domain.Model.Base
{
    public class ValidatioMessageResult
    {
        public ValidatioMessageResult(string message, ErrorCode errorCode)
        {
            Message = message;
            ErrorCode = errorCode;
        }

        public string Message { get; set; }
        public ErrorCode ErrorCode { get; set; }
    }
}