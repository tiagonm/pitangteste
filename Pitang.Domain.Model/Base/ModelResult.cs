﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Pitang.Domain.Model.Base
{
    public class ModelResult<TModel> : ModelResultValidation  where TModel : IModel
    {
        public TModel Model { get; private set; }

        public void SetModel(TModel model)
        {
            this.Model = model;
        }
    }
}
