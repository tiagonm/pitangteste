﻿using Pitang.Domain.Model.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pitang.Domain.Model.Base
{
    public class ModelResultValidation
    {
        public bool IsValid { get; set; } = true;
        public List<ValidatioMessageResult> Validations { get; } = new List<ValidatioMessageResult>();

        public void AddValidation(string message, ErrorCode errorCorde)
        {
            var validation = new ValidatioMessageResult(message, errorCorde);
            this.Validations.Add(validation);
            this.IsValid = false;
        }
    }
}
