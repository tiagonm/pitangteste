﻿using Microsoft.EntityFrameworkCore;
using Pitang.Domain.Model;
using Pitang.Infrastucture.Repository;
using Pitang.Infrastucture.Repository.Implementation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Pitang.Tests.Unit.Repository
{
    public class UserRepositoryTest
    {
        protected PitangDbContext contextInMemory;
        protected UserRepository _userRepositoryMock;
        

        public UserRepositoryTest()
        {
            SetupContext();
            _userRepositoryMock = new UserRepository(contextInMemory);
                       
        }

        [Fact]
        public async Task get_user_by_id()
        {
           var user = await _userRepositoryMock.GetById(1);

            Assert.NotNull(user);            
        }

        private void SetupContext()
        {
            var options = new DbContextOptionsBuilder<PitangDbContext>()
                              .UseInMemoryDatabase(Guid.NewGuid().ToString())
                              .Options;
            contextInMemory = new PitangDbContext(options);

            var user1 = new User();
            user1.Id = 1;
            user1.FirstName = "Tiago";
            user1.LastName = "Marques";
            user1.Password = "senha";
            user1.Phone = new List<UserPhone>();


            contextInMemory.Users.Add(user1);
            contextInMemory.SaveChanges();
        }
    }
}
