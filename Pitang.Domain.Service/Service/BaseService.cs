﻿using Pitang.Infrastucture.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pitang.Domain.Service.Service
{
    public class BaseService
    {
        protected IUnitOfWork _dbContext { get; private set; }

        public BaseService(IUnitOfWork dbContext)
        {
            this._dbContext = dbContext;            
        }
    }
}
