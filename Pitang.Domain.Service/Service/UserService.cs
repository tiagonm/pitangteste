﻿using Pitang.Domain.Dto;
using Pitang.Domain.Model;
using Pitang.Domain.Model.Base;
using Pitang.Domain.Service.Contracts;
using Pitang.Infrastucture.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Pitang.Domain.Service.Util;
using Pitang.Domain.Model.Enum;

namespace Pitang.Domain.Service.Service
{
    public class UserService : BaseService, IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUnitOfWork dbContext, IUserRepository userRepository) : base(dbContext)
        {
            _userRepository = userRepository;
        }

        public async Task<ModelResultValidation> Create(UserDto userDto)
        {
            var result = new ModelResultValidation();
            var password = EncryptService.Encrypt(userDto.Password);
            var phones = GetPhonesModel(result,userDto.Phone);
            var existsEmail = await _userRepository.GetByEmail(userDto.Email);

            if (existsEmail)
                result.AddValidation("E-mail already exists", ErrorCode.BusinessError);

            var user = User.Create(result, userDto.Email, userDto.FirstName, userDto.LastName, password, phones);

            if (result.IsValid)
            {
                await this._userRepository.AddAsync(user);
                await this._dbContext.CompleteAsync();
            }

            return result;
        }

        public async Task<ModelResult<User>> GetById(int id)
        {
            var result = new ModelResult<User>();

            var user = await _userRepository.GetById(id);

            if (user != null)
                result.SetModel(user);
            else
                result.AddValidation("User not found.", ErrorCode.NotFound);

            return result;
        }

        public async Task<ModelResult<User>> SignIn(string email, string password)
        {
            var result = new ModelResult<User>();
            var passwordEncrypt = EncryptService.Encrypt(password);

            var user = await _userRepository.SignIn(email, passwordEncrypt);

            if (user != null)
            {
                result.SetModel(user);

                user.LastLogin = DateTime.Now;

                await this._dbContext.CompleteAsync();
            }
            else
                result.AddValidation("Invalid e-mail or password.", ErrorCode.NotFound);

            return result;
        }

        private List<UserPhone> GetPhonesModel(ModelResultValidation result,List<PhoneDto> phonesDto)
        {
            var phones = new List<UserPhone>();

            foreach (var phone in phonesDto)
            {
                var userPhone = UserPhone.Create(result,phone.AreaCode,phone.CountryCode,phone.Number);
                phones.Add(userPhone);
            }

            return phones;
        }
    }
}
