﻿using Pitang.Domain.Dto;
using Pitang.Domain.Model;
using Pitang.Domain.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Pitang.Domain.Service.Contracts
{
    public interface IUserService
    {
        Task<ModelResultValidation> Create(UserDto user);

        Task<ModelResult<User>> GetById(int id);

        Task<ModelResult<User>> SignIn(string email,string password);
    }
}
