﻿using Pitang.Application.Messages.Message;
using Pitang.Domain.Dto;
using Pitang.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pitang.Application.Mapper.Mappers
{
    public class UserMapper
    {
        public static UserDto ToUserDto(UserMessageRequest request)
        {
            List<PhoneDto> phonesDto = PhoneMapper.ToPhoneDto(request.Phones);

            return new UserDto(request.FirstName, request.LastName, request.Email, request.Password, phonesDto);
        }

        public static UserMessageResponse ToUserMessageResponse(User user)
        {
            List<PhoneMessageRequest> phonesMessage = PhoneMapper.ToPhoneMessageResponse(user.Phone);
            var userMessage = new UserMessageResponse();

            userMessage.Created_At = user.CreatedAt;
            userMessage.Email = user.Email;
            userMessage.FirstName = user.FirstName;
            userMessage.LastName = user.LastName;
            userMessage.Last_Login = user.LastLogin;
            userMessage.Phones = phonesMessage;

            return userMessage;
        }
    }
}
