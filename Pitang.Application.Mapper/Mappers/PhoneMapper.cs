﻿using System;
using System.Collections.Generic;
using Pitang.Application.Messages.Message;
using Pitang.Domain.Dto;
using Pitang.Domain.Model;

namespace Pitang.Application.Mapper.Mappers
{
    internal class PhoneMapper
    {
        internal static List<PhoneDto> ToPhoneDto(List<PhoneMessageRequest> phones)
        {
            var phonesList = new List<PhoneDto>();

            foreach (var phone in phones)
            {
                phonesList.Add(new PhoneDto(phone.Number, phone.Area_Code, phone.Country_Code));
            }

            return phonesList;
        }

        internal static List<PhoneMessageRequest> ToPhoneMessageResponse(List<UserPhone> phones)
        {
            var phonesList = new List<PhoneMessageRequest>();

            foreach (var phone in phones)
            {
                phonesList.Add(new PhoneMessageRequest { Area_Code = phone.AreaCode, Country_Code = phone.CountryCode, Number = phone.Number });
            }

            return phonesList;
        }
    }
}