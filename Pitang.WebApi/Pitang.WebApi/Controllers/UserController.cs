﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pitang.Application.Mapper.Mappers;
using Pitang.Application.Messages.Message;
using Pitang.Domain.Dto;
using Pitang.Domain.Service.Contracts;
using Pitang.WebApi.Base;
using Pitang.WebApi.Sigin;

namespace Pitang.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService,
            TokenConfigurations tokenConfigurations,
            SigningConfigurations signingConfigurations)
            : base(tokenConfigurations, signingConfigurations)
        {
            this._userService = userService;
        }

        [AllowAnonymous, HttpPost("signup")]
        public async Task<IActionResult> SignUp([FromBody] UserMessageRequest request)
        {
            var userMessageResponse = new SignupMessageResponse();
            var userDto = UserMapper.ToUserDto(request);

            var result = await this._userService.Create(userDto);

            return GerarResultAPartirModel(userMessageResponse, result);
        }

        [AllowAnonymous, HttpPost("sigin")]
        public async Task<IActionResult> SignIn([FromBody] SiginMessageRequest request)
        {
            var siginMessageResponse = new SiginMessageResponse();

            var result = await this._userService.SignIn(request.Email, request.Password);

            if (result.IsValid)
            {
                siginMessageResponse.AccessToken = base.GenerateDominioToken(result.Model);
            }

            return GerarResultAPartirModel(siginMessageResponse, result);
        }

        [Authorize("Bearer"), HttpGet("me")]
        public async Task<IActionResult> GetUser()
        {
            var userMessageResponse = new UserMessageResponse();
            var userId = GetCurrentUserId();

            var result = await this._userService.GetById(userId);

            if (result.IsValid)
            {
                userMessageResponse = UserMapper.ToUserMessageResponse(result.Model);
            }

            return GerarResultAPartirModel(userMessageResponse, result);
        }
    }
}