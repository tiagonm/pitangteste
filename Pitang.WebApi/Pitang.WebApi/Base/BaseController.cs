﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Pitang.Application.Messages.Base;
using Pitang.Application.Messages.Message;
using Pitang.Domain.Model;
using Pitang.Domain.Model.Base;
using Pitang.WebApi.Sigin;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Pitang.WebApi.Base
{
    public class BaseController : ControllerBase
    {
        private TokenConfigurations _tokenConfigurations;
        private SigningConfigurations _signingConfigurations;
        public BaseController(TokenConfigurations tokenConfigurations, SigningConfigurations signingConfigurations)
        {
            _tokenConfigurations = tokenConfigurations;
            _signingConfigurations = signingConfigurations;
        }
        protected IActionResult GerarResultAPartirModel<TResponse>(TResponse result, ModelResultValidation modelResult)
           where TResponse : BaseMessageResponse
        {
            IActionResult actionResult;

            if (!modelResult.IsValid)
            {
                result.ErrorCode = (int)modelResult.Validations[0].ErrorCode;
                result.Message = modelResult.Validations[0].Message;

                if (result.ErrorCode == 1)
                    actionResult = NotFound(result);
                else
                    actionResult = UnprocessableEntity(result);
            }
            else
            {
                
                result.Message = "Success!";
                actionResult = Ok(result);
            }

            return actionResult;
        }

        public string GenerateDominioToken(User user)
        {
            ClaimsIdentity identity = new ClaimsIdentity(
                    new GenericIdentity(user.Id.ToString(), "Login"),
                    new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, user.Id.ToString())
                    }
                );

            DateTime dataCriacao = DateTime.Now;
            DateTime dataExpiracao = dataCriacao +
                TimeSpan.FromSeconds(_tokenConfigurations.Seconds);

            var handler = new JwtSecurityTokenHandler();
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _tokenConfigurations.Issuer,
                Audience = _tokenConfigurations.Audience,
                SigningCredentials = _signingConfigurations.SigningCredentials,
                Subject = identity,
                NotBefore = dataCriacao,
                Expires = dataExpiracao
            });
            var token = handler.WriteToken(securityToken);
            return token;
        }

        protected int GetCurrentUserId()
        {
            var user = this.User;
            if (user.Claims.Count() > 0)
            {
                return Convert.ToInt32(this.User.Claims.ToList()[0].Value);
            }

            return 0;
        }
    }
}
